clc;
clear all;
[m,n] = size(A);
if nargin < 3
    t0 = ones(n,1);
    niter = 10;
elseif nargin < 4
    niter = 10;
end
if isempty(t0)
    t0 = ones(n,1);
elseif any(t0 <= 0)
    disp('Error. The initial vector should be nonzero');
    t = [];
    y = [];
    return
end
AtA = A'*A;
Atb = A'*b;
In = eye(n);
e = ones(n,1);
%Step 0
y0 = x0;
k = 0;
TOL1 = n*eps;
TOL2 = n*sqrt(eps);
tk = t0;
yk = y0;
noready = 1;
while noready
    
    k= k+1;
    
    %Step 1
    Tk = diag(tk);
    Yk = diag(yk);
    C = [Tk Yk; AtA -In];
    d = [-Tk*Yk*e; -AtA*Tk*e+Yk*e+Atb];
    uv = C\d;
    uk(:,1) = uv(1:n);
    vk(:,1) = uv(n+1:2*n);
    
    %Step 2
    T1 = min(-tk(uk < 0) ./ uk(uk < 0)); 
    T2 = min(-yk(vk < 0) ./ vk(vk < 0));
    theta = 0.99995 * min(T1,T2);
    if isempty(theta)
        noready = 0;
        theta = 0;
    end
    
    %Step 3
    mk = (tk + theta*uk)'*(yk + theta*vk) / (n^2);
    Uk = diag(uk);
    Vk = diag(vk);
    d = [-Tk*Yk*e+mk*e-Uk*Vk*e; -AtA*Tk*e+Yk*e+Atb];
    zw = C\d;
    zk(:,1) = zw(1:n);
    wk(:,1) = zw(n+1:2*n);
    
    %Step 4
    T1 = min(-tk(zk < 0) ./ zk(zk < 0)); 
    T2 = min(-yk(wk < 0) ./ wk(wk < 0));
    theta = 0.99995 * min(T1,T2);
    if isempty(theta)
        theta = 0;
        noready = 0;
    end
    tk = tk + theta*zk;
    yk = yk + theta*wk;
    
    %Step 5
    if (tk'*yk < TOL1) & (norm(AtA*xk-Atb-yk) < TOL2)
        noready = 0;
    elseif k == niter
        noready = 0;
    end
    
end
t = tk;
y = yk;

