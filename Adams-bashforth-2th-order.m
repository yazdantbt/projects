clc;
clear all;
main1=4*t*y
main2=1+t^2
f = inline('main1/main2','t','y');
xrange=[0,2];
h=0.1;
t=1:h:2;
n = (xrange(2)-xrange(1))/h;
y(0) = 1;
% generate starting estimates using Runge-Kutta
for i = 1
	k1 = f(t(i), y(i));
	k2 = f(t(i) + h/2, y(i) + h/2*k1);
	k3 = f(t(i) + h/2, y(i) + h/2*k2);
	k4 = f(t(i) + h, y(i) + h*k3);
	y(i+1) = y(i) + h/6*(k1 + 2*k2 + 2*k3 + k4);
	t(i+1) = t(i) + h;
end
for i = 3:n+1
	y(i) = y(i-1) + h/12*(5*f(t(i),y(i)) + 8*f(t(i-1),y(i-1))- f(t(i-2),y(i-2)));
end
